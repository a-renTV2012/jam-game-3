﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    [Header("Меню настроек")]
    public RectTransform currentPanel;
    public RectTransform[] panels;
    public MouseLook MouseLook;
    public ReflectionProbe ReflectionProbe;
    public AudioListener AudioListener;
    public bool open = false;


    [Header("Настройки звука")]
    public Slider VolSlider;

    [Header("Настройки яркости")]
    public Slider BrtSlider;

    [Header("Настройки чувствительности")]
    public Slider SensSlider;


    private void hidePanel() {
        currentPanel.offsetMin = new Vector2(currentPanel.offsetMin.x, 1050);
        currentPanel.offsetMax = new Vector2(currentPanel.offsetMax.x, 1050);
    }


    private void showPanel() {
        currentPanel.offsetMin = new Vector2(currentPanel.offsetMin.x, -129);
        currentPanel.offsetMax = new Vector2(currentPanel.offsetMax.x, -129);
    }


    public void Start()
    {
        if (SceneManager.GetActiveScene().name == "Maze") {
            MouseLook = GameObject.Find("Player").GetComponent<MouseLook>();
            ReflectionProbe = GameObject.Find("Player").transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head/MainCamera/ReflectionProbe").GetComponent<ReflectionProbe>();
            AudioListener = GameObject.Find("Player").transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head/MainCamera/ReflectionProbe").GetComponent<AudioListener>();
        }
        
        Load();
        Set();
    }


    public void Save()
    {
        PlayerPrefs.SetFloat("Volume", VolSlider.value);
        PlayerPrefs.SetFloat("Sensitivity", SensSlider.value);
        PlayerPrefs.SetFloat("Brightness", BrtSlider.value);
        PlayerPrefs.Save();
    }


    public void Set()
    {
        if (MouseLook != null && ReflectionProbe != null && AudioListener != null) {
            if (PlayerPrefs.HasKey("Sensitivity"))
            {
                MouseLook.sensitivity = PlayerPrefs.GetFloat("Sensitivity");
            }
            else
            {
                MouseLook.sensitivity = 9;
            }

            if (PlayerPrefs.HasKey("Volume"))
            {
                AudioListener.volume = PlayerPrefs.GetFloat("Volume");
            }
            else
            {
                AudioListener.volume = 0.5f;
            }

            if (PlayerPrefs.HasKey("Brightness"))
            {
                ReflectionProbe.intensity = PlayerPrefs.GetFloat("Brightness");
            }
            else
            {
                ReflectionProbe.intensity = 0;
            }
        }
    }


    public void Load()
    {
        if (PlayerPrefs.HasKey("Volume"))
        {
            VolSlider.value = PlayerPrefs.GetFloat("Volume");
        }
        else
        {
            VolSlider.value = 0.5f;
        }

        if (PlayerPrefs.HasKey("Sensitivity"))
        {
            SensSlider.value = PlayerPrefs.GetFloat("Sensitivity");
        }
        else
        {
            SensSlider.value = 9;
        }

        if (PlayerPrefs.HasKey("Brightness"))
        {
            BrtSlider.value = PlayerPrefs.GetFloat("Brightness");
        }
        else
        {
            BrtSlider.value = 0;
        }
    }


    public void Link(string link)
    {
        Application.OpenURL(link);
    }


    public void LoadScene(int indexScene)
    {
        SceneManager.LoadScene(indexScene);
    }


    public void Exit()
    {
        Application.Quit();
    }


    public void changeMenuState(int pan)
    {
        if (currentPanel != null) {
            hidePanel();

            if (currentPanel != panels[pan] || !open) {
                currentPanel = panels[pan];

                showPanel();

                open = true;
            } else {
                open = false;
            }
        } else {
            currentPanel = panels[pan];

            showPanel();

            open = true;
        }
    }
}