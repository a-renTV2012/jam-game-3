﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameUI : MonoBehaviour
{
    private int MaxHealthBarWidth = 400;
    private int MaxStaminaBarWidth = 360;

    public GameObject PauseCanvas;
    public GameObject ContinueButton;
    public GameObject RestartButton;
    public GameObject MainCanvas;
    public RectTransform HealthBar;
    public RectTransform StaminaBar;
    public GameObject Player;
    public PlayerProperties PlayerProperties;
    public AudioListener AudioListener;


    private void TogglePause() {
        if (Time.timeScale == 0) {
            Time.timeScale = 1.0f;

            AudioListener.volume = PlayerPrefs.GetFloat("Volume");

        } else {
            Time.timeScale = 0;

            AudioListener.volume = 0;
        }
    }

    void Start()
    {
        PauseCanvas = GameObject.Find("PauseCanvas");
        ContinueButton = PauseCanvas.transform.Find("bttGridPanel/continueButton").gameObject;
        RestartButton = PauseCanvas.transform.Find("bttGridPanel/restartButton").gameObject;
        PauseCanvas.SetActive(false);

        MainCanvas = GameObject.Find("MainCanvas");
        HealthBar = MainCanvas.transform.Find("HealthBarBackground/HealthBar").gameObject.GetComponent<RectTransform>();
        StaminaBar = MainCanvas.transform.Find("StaminaBarBackground/StaminaBar").gameObject.GetComponent<RectTransform>();

        Player = GameObject.Find("Player");
        PlayerProperties = Player.GetComponent<PlayerProperties>();

        AudioListener = Player.transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head/MainCamera/ReflectionProbe").GetComponent<AudioListener>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            ChangeMainMenuState();
        }
    }

    public void ChangeMainMenuState() {
        PauseCanvas.SetActive(!PauseCanvas.activeSelf);

        Cursor.visible = !Cursor.visible;

        if (Cursor.lockState == CursorLockMode.Locked) {
            Cursor.lockState = CursorLockMode.None;
        } else {
            Cursor.lockState = CursorLockMode.Locked;
        }

        TogglePause();
    }

    public void TogglePrimaryButton() {
        ContinueButton.SetActive(!ContinueButton.activeSelf);
        RestartButton.SetActive(!RestartButton.activeSelf);
    }

    public void SetHealthBarWidth(float Health) {
        HealthBar.sizeDelta = new Vector2(MaxHealthBarWidth / PlayerProperties.MaxHealth * Health, 48);
    }

    public void SetStaminaBarWidth(float Stamina) {
        StaminaBar.sizeDelta = new Vector2(MaxStaminaBarWidth / PlayerProperties.MaxStamina * Stamina, 24);
    }
}
