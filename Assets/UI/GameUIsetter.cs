﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUIsetter : MonoBehaviour
{
    public MouseLook MouseLook;
    public ReflectionProbe ReflectionProbe;
    public AudioListener AudioListener;


    void Start()
    {
        MouseLook = GameObject.Find("Player").GetComponent<MouseLook>();
        ReflectionProbe = GameObject.Find("Player").transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head/MainCamera/ReflectionProbe").GetComponent<ReflectionProbe>();
        AudioListener = GameObject.Find("Player").transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head/MainCamera/ReflectionProbe").GetComponent<AudioListener>();
        
        LoadSettings();

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }


    void LoadSettings()
    {
        if (PlayerPrefs.HasKey("Sensitivity"))
        {
            MouseLook.sensitivity = PlayerPrefs.GetFloat("Sensitivity");
        }
        else
        {
            MouseLook.sensitivity = 9;
        }

        if (PlayerPrefs.HasKey("Volume"))
        {
            AudioListener.volume = PlayerPrefs.GetFloat("Volume");
        }
        else
        {
            AudioListener.volume = 0.5f;
        }

        if (PlayerPrefs.HasKey("Brightness"))
        {
            ReflectionProbe.intensity = PlayerPrefs.GetFloat("Brightness");
        }
        else
        {
            ReflectionProbe.intensity = 0;
        }
    }
}
