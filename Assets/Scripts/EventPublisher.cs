﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable] public class NoteInteractionEvent : UnityEvent<string> {}
[System.Serializable] public class DamageTypeChangeEvent : UnityEvent<string> {}
[System.Serializable] public class MeleeDamageChangeEvent : UnityEvent<float> {}


public class EventPublisher : MonoBehaviour
{
    public NoteInteractionEvent noteInteractionEvent;
    public DamageTypeChangeEvent DamageTypeChangeEvent;
    public MeleeDamageChangeEvent MeleeDamageChangeEvent;


    public void invokeNoteInteractionEvent(string name) {
        noteInteractionEvent.Invoke(name);
    }

    public void InvokeDamageTypeChangeEvent(string type) {
        DamageTypeChangeEvent.Invoke(type);
    }

    public void InvokeMeleeDamageChangeEvent(float value) {
        MeleeDamageChangeEvent.Invoke(value);
    }
}


