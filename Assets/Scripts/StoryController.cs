﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoryController : MonoBehaviour
{
    public int progress = 0;
    public int maxProgress = 2;

    
    public TextAsset getNote(int number = 0) {
        if (number == 0) {
            progress += 1;
            number = progress;
        }

        return Resources.Load<TextAsset>("Notes/" + number);
    }
}
