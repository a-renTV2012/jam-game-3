﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boost : MonoBehaviour
{
    private EventPublisher _EventPublisher;
    public string BoostType = "";

    void Start() {
        _EventPublisher = GameObject.FindObjectOfType<EventPublisher>();
    }

    void OnTriggerEnter(Collider Collider) {
        if (Collider.transform.name == "Player") {
            if (BoostType == "Health") {
                Collider.gameObject.GetComponent<PlayerProperties>().MaxHealth += 50;
                Collider.gameObject.GetComponent<PlayerProperties>().health = Collider.gameObject.GetComponent<PlayerProperties>().MaxHealth;

            } else if (BoostType == "Stamina") {
                Collider.gameObject.GetComponent<PlayerProperties>().MaxStamina += 50;
                Collider.gameObject.GetComponent<PlayerProperties>().stamina = Collider.gameObject.GetComponent<PlayerProperties>().MaxStamina;
                Collider.gameObject.GetComponent<PlayerProperties>().StaminaRechargeRate += 2.5f;
                Collider.gameObject.GetComponent<PlayerProperties>().StaminaRechargeCooldown -= 0.5f;

            } else if (BoostType == "Agility") {
                Collider.gameObject.GetComponent<PlayerProperties>().crouchSpeed += 2;
                Collider.gameObject.GetComponent<PlayerProperties>().jumpSpeed += 5;
                Collider.gameObject.GetComponent<PlayerProperties>().boundSpeed += 5;
                Collider.gameObject.GetComponent<PlayerProperties>().BoundTime -= 0.1f;
                Collider.gameObject.GetComponent<PlayerProperties>().LightStrikeTime -= 0.2f;
                Collider.gameObject.GetComponent<PlayerProperties>().HardStrikeTime -= 0.4f;
                Collider.gameObject.GetComponent<PlayerProperties>().CrossbowLoadTime -= 0.6f;

            } else if (BoostType == "Speed") {
                Collider.gameObject.GetComponent<PlayerProperties>().moveSpeed += 2;
                Collider.gameObject.GetComponent<PlayerProperties>().sprintSpeed += 4;

            } else if (BoostType == "DamageType") {
                _EventPublisher.InvokeDamageTypeChangeEvent("Enchanted");

            } else if (BoostType == "Damage") {
                _EventPublisher.InvokeMeleeDamageChangeEvent(1.2f);
                
            }

            Object.Destroy(gameObject);
        } 
    }
}
