﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SharpBolt : Bolt, IItem
{
    public override float BaseDamage {get;set;} = 135;
}
