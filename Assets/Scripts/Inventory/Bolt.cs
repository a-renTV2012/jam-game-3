﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bolt : MonoBehaviour, IItem, IWeapon
{
    protected GameObject _Storage;
    protected PlayerInventory _PlayerInventory;
    protected EventPublisher _EventPublisher;

    public int Quantity {get; set;} = 1;
    public bool Striking {get; set;} = false;
    public virtual float BaseDamage {get; set;} = 100;
    public float CurrentDamageMultiplier {get; set;} = 1;
    public float MaxDamageMultiplier {get; set;} = 1;
    public string StrikeType {get;} = "Piercing";
    public string DamageType {get; set;} = "Physical";

    
    protected void OnDamageTypeChange(string type) {
        DamageType = type;
    }
    
    void Start() {
        _Storage = GameObject.Find("Player").transform.Find("Storage").gameObject;
        _PlayerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();
        _EventPublisher = GameObject.FindObjectOfType<EventPublisher>();

        _EventPublisher.DamageTypeChangeEvent.AddListener(OnDamageTypeChange);
    }

    public void OnKeyDown() {}

    public void OnKey() {}

    public void OnKeyUp() {}

    public void PlaceInStorage() {
        transform.parent = _Storage.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
    }

    public void PlaceInHand(GameObject Hand) {}

    public void AddToInventory() {
        if (!_PlayerInventory.Items.ContainsKey(transform.name)) {
            PlaceInStorage();
            _PlayerInventory.Items.Add(transform.name, this);
            gameObject.tag = "Item";
            gameObject.GetComponent<BoxCollider>().isTrigger = false;
        } else {
            _PlayerInventory.Items[transform.name].Quantity += 1;
            Object.Destroy(transform.gameObject);
        }
    }

    public void RemoveFromInventory(Transform NewParent = null) {
        if (Quantity == 1) {
            transform.parent = NewParent;
            _PlayerInventory.Items.Remove(transform.name);
            gameObject.tag = "Interactable";
            gameObject.GetComponent<BoxCollider>().isTrigger = true;

            if (NewParent.name == "Crossbow") {
                transform.name = "Bolt";
            }
        } else {
            Quantity -= 1;
            GameObject NewBolt = Object.Instantiate(transform.gameObject);
            NewBolt.tag = "Interactable";
            NewBolt.transform.position = new Vector3(NewBolt.transform.position.x, NewBolt.transform.position.y - 2, NewBolt.transform.position.z);
            NewBolt.transform.parent = NewParent;
            NewBolt.GetComponent<BoxCollider>().isTrigger = true;

            if (NewParent.name == "Crossbow") {
                NewBolt.transform.name = "Bolt";
            }
        }
        
    }
}
