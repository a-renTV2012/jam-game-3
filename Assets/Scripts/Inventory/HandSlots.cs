﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandSlots : MonoBehaviour
{
    private PlayerInventory _PlayerInventory;
    public string[] Slots = new string[2];
    

    public void CycleSlots() {
        Dictionary<string, IItem> Items = _PlayerInventory.Items;
        
        Items[Slots[0]].PlaceInStorage();

        string tmp = Slots[1];
        Slots[1] = Slots[0];
        
        Items[tmp].PlaceInHand(transform.gameObject);
    }

    void Start() {
        _PlayerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();

        //Slots[0] = transform.name;
        //Slots[1] = transform.name;
    }
}