﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItem
{
    Transform transform {get;}

    GameObject gameObject {get;}

    int Quantity {get; set;}

    void OnKeyDown();

    void OnKey();

    void OnKeyUp();

    void PlaceInStorage();

    void PlaceInHand(GameObject Hand);

    void AddToInventory();

    void RemoveFromInventory(Transform NewParent = null);
}
