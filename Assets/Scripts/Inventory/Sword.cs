﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour, IItem, IWeapon
{
    private GameObject _Storage;
    private PlayerInventory _PlayerInventory;
    private PlayerProperties _PlayerProperties;
    private EventPublisher _EventPublisher;
    private Animator _Animator;
    private AudioSource _AudioSource;
    private float _CurrentDamageMultiplier;

    public int Quantity {get; set;} = 1;
    public bool Striking {get; set;} = false;
    public float BaseDamage {get; set;} = 50;
    public float CurrentDamageMultiplier {
        get {
            return _CurrentDamageMultiplier;
        } 
        
        set {
            if (value < 0) {
                _CurrentDamageMultiplier = 0;
            } else if (value > MaxDamageMultiplier) {
                _CurrentDamageMultiplier = MaxDamageMultiplier;
            } else {
                _CurrentDamageMultiplier = value;
            }
        }
    }
    public float MaxDamageMultiplier {get;} = 3;
    public string StrikeType {get; set;} = "Chopping";
    public string DamageType {get; set;} = "Physical";


    private void OnMeleeDamageChange(float Multiplier) {
        BaseDamage *= Multiplier;
    }

    private void OnDamageTypeChange(string type) {
        DamageType = type;
    }
    
    void Start() {
        _Storage = GameObject.Find("Player").transform.Find("Storage").gameObject;
        _PlayerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();
        _PlayerProperties = GameObject.Find("Player").GetComponent<PlayerProperties>();
        _EventPublisher = GameObject.FindObjectOfType<EventPublisher>();

        _EventPublisher.MeleeDamageChangeEvent.AddListener(OnMeleeDamageChange);
        _EventPublisher.DamageTypeChangeEvent.AddListener(OnDamageTypeChange);

        _Animator = GameObject.Find("Player").GetComponent<Animator>();

        _AudioSource = GetComponent<AudioSource>();

        // TEMP!!!
        DamageType = "Enchanted";
    }

    public void OnKeyDown() {
        CurrentDamageMultiplier = 1;
    }

    public void OnKey() {
        CurrentDamageMultiplier += 1 * Time.deltaTime;
    }

    public void OnKeyUp() {
        if (CurrentDamageMultiplier < 2) {
            StrikeType = "Chopping";

            if (!Striking) {
                StartCoroutine("LightStrikeAnimation");
            }
        } else {
            StrikeType = "Piercing";

            if (!Striking) {
                StartCoroutine("HardStrikeAnimation");
            }
        }
    }

    public void PlaceInStorage() {
        if (transform.parent != null) {
            if (transform.parent.gameObject.GetComponent<HandSlots>() != null) {
                transform.parent.gameObject.GetComponent<HandSlots>().Slots[0] = transform.parent.transform.name;
            }
        }

        transform.parent = _Storage.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
    }

    public void PlaceInHand(GameObject Hand) {
        Hand.GetComponent<HandSlots>().Slots[0] = transform.name;
        transform.parent = Hand.transform;

        if (Hand.transform.name == "LeftHand") {
            transform.localRotation = Quaternion.Euler(75, 0, 90);
            transform.localPosition = new Vector3(0.25f, -9, 2.4f);
        } else {
            transform.localRotation = Quaternion.Euler(3, -92, -84);
            transform.localPosition = new Vector3(-9, -0.1f, 0);
        }
    }

    public void AddToInventory() {
        PlaceInStorage();
        _PlayerInventory.Items.Add(transform.name, this);
        gameObject.tag = "Item";
        gameObject.GetComponent<BoxCollider>().isTrigger = false;
    }

    public void RemoveFromInventory(Transform NewParent = null) {
        transform.parent = NewParent;
        _PlayerInventory.Items.Remove(transform.name);
        gameObject.tag = "Interactable";
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
    }

    IEnumerator LightStrikeAnimation() {
        _Animator.SetBool(transform.parent.name + "Striking", true);
        
        yield return new WaitForSeconds(_PlayerProperties.LightStrikePrepareTime);

        Striking = true;
        _AudioSource.Play();

        yield return new WaitForSeconds(_PlayerProperties.LightStrikeTime);

        Striking = false;
        _Animator.SetBool(transform.parent.name + "Striking", false);
    }

    IEnumerator HardStrikeAnimation() {
        _Animator.SetBool(transform.parent.name + "Striking", true);

        yield return new WaitForSeconds(_PlayerProperties.HardStrikePrepareTime);

        Striking = true;
        _AudioSource.Play();

        yield return new WaitForSeconds(_PlayerProperties.HardStrikeTime);

        Striking = false;
        _Animator.SetBool(transform.parent.name + "Striking", false);
    }
}
