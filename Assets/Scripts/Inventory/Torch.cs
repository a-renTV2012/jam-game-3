﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Torch : MonoBehaviour, IItem, IWeapon
{
    private GameObject _Storage;
    private PlayerInventory _PlayerInventory;
    private PlayerProperties _PlayerProperties;
    private EventPublisher _EventPublisher;
    private Animator _Animator;
    private float _CurrentDamageMultiplier;

    public int Quantity {get; set;} = 1;
    public bool Striking {get; set;} = false;
    public int timer = 120;
    public float fireChangeTick = 0.025f;
    public float BaseDamage {get; set;} = 50;
    public float CurrentDamageMultiplier {
        get {
            return _CurrentDamageMultiplier;
        } 
        
        set {
            if (value < 0) {
                _CurrentDamageMultiplier = 0;
            } else if (value > MaxDamageMultiplier) {
                _CurrentDamageMultiplier = MaxDamageMultiplier;
            } else {
                _CurrentDamageMultiplier = value;
            }
        }
    }
    public float MaxDamageMultiplier {get;} = 2;
    public string StrikeType {get;} = "Common";
    public string DamageType {get; set;} = "Fire";
    public Light torchLight;
    public ParticleSystem torchFire;
    public AudioSource fireSound;


    private void OnMeleeDamageChange(float Multiplier) {
        BaseDamage *= Multiplier;
    }
    
    void Start()
    {
        _Storage = GameObject.Find("Player").transform.Find("Storage").gameObject;
        _PlayerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();
        _PlayerProperties = GameObject.Find("Player").GetComponent<PlayerProperties>();
        _EventPublisher = GameObject.FindObjectOfType<EventPublisher>();

        _EventPublisher.MeleeDamageChangeEvent.AddListener(OnMeleeDamageChange);

        _Animator = GameObject.Find("Player").GetComponent<Animator>();

        torchLight = transform.Find("TorchFire").transform.Find("TorchLight").gameObject.GetComponent<Light>();
        torchFire = transform.Find("TorchFire").gameObject.GetComponent<ParticleSystem>();
        fireSound = transform.Find("TorchFire").gameObject.GetComponent<AudioSource>();
        fireSound.clip = Resources.Load<AudioClip>("Audio/Fire");

        if (transform.parent != null) {
            if (transform.parent.GetComponent<Hand>() != null) {
                enableTorch();
            }
        }
    }

    public void OnKeyDown() {
        CurrentDamageMultiplier = 1;
    }

    public void OnKey() {
        CurrentDamageMultiplier += 1 * Time.deltaTime;
    }

    public void OnKeyUp() {
        if (CurrentDamageMultiplier < 1.5f) {
            if (!Striking) {
                StartCoroutine("LightStrikeAnimation");
            }
        } else {
            if (!Striking) {
                StartCoroutine("HardStrikeAnimation");
            }
        }
    }

    public void PlaceInStorage() {
        disableTorch();

        if (transform.parent != null) {
            if (transform.parent.gameObject.GetComponent<HandSlots>() != null) {
                transform.parent.gameObject.GetComponent<HandSlots>().Slots[0] = transform.parent.transform.name;
            }
        }

        transform.parent = _Storage.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
    }

    public void PlaceInHand(GameObject Hand) {
        Hand.GetComponent<HandSlots>().Slots[0] = transform.name;
        transform.parent = Hand.transform;

        if (Hand.transform.name == "LeftHand") {
            transform.localRotation = Quaternion.Euler(194, -126, 16);
            transform.localPosition = new Vector3(1.74f, 0.57f, 1.48f);
        } else {
            transform.localRotation = Quaternion.Euler(74, 0, 0);
            transform.localPosition = new Vector3(-0.2f, -3, 0.4f);
        }

        enableTorch();
    }

    public void AddToInventory() {
        PlaceInStorage();
        _PlayerInventory.Items.Add(transform.name, this);
        gameObject.tag = "Item";
        gameObject.GetComponent<BoxCollider>().isTrigger = false;
    }

    public void RemoveFromInventory(Transform NewParent = null) {
        transform.parent = NewParent;
        _PlayerInventory.Items.Remove(transform.name);
        gameObject.tag = "Interactable";
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
    }

    void turnOnLight() {
        torchLight.range += 1;

        if (torchLight.range == 25) {
            CancelInvoke("turnOnLight");
        }
    }

    void turnOffLight() {
        torchLight.range -= 1;

        if (torchLight.range == 0) {
            CancelInvoke("turnOffLight");
        }
    }

    void decreaseTimer() {
        timer -= 1;

        if (timer == 0) {
            disableTorch();
        }
    }

    void disableTorch() {
        InvokeRepeating("turnOffLight", 0, fireChangeTick);
        torchFire.Stop();
        fireSound.Stop();
        CancelInvoke("decreaseTimer");
    }

    void enableTorch() {
        InvokeRepeating("turnOnLight", 0, fireChangeTick);
        torchFire.Play();
        fireSound.Play();
        InvokeRepeating("decreaseTimer", 0, 1f);
    }

    public void StartTorchEnablingCoroutine() {
        StartCoroutine("enableTorchAfterFiringAnimation");
    }

    public void StartTorchDisablingCoroutine() {
        StartCoroutine("disableTorchAfterFiringAnimation");
    }

    IEnumerator LightStrikeAnimation() {
        _Animator.SetBool(transform.parent.name + "Striking", true);

        yield return new WaitForSeconds(_PlayerProperties.LightStrikePrepareTime);

        Striking = true;

        yield return new WaitForSeconds(_PlayerProperties.LightStrikeTime);

        Striking = false;
        _Animator.SetBool(transform.parent.name + "Striking", false);
    }

    IEnumerator HardStrikeAnimation() {
        _Animator.SetBool(transform.parent.name + "Striking", true);

        yield return new WaitForSeconds(_PlayerProperties.HardStrikePrepareTime);

        Striking = true;

        yield return new WaitForSeconds(_PlayerProperties.HardStrikeTime);

        Striking = false;
        _Animator.SetBool(transform.parent.name + "Striking", false);
    }

    IEnumerator enableTorchAfterFiringAnimation() {
        yield return new WaitForSeconds(2);

        enableTorch();
    }

    IEnumerator disableTorchAfterFiringAnimation() {
        yield return new WaitForSeconds(2);

        disableTorch();
    }
}
