﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crossbow : MonoBehaviour, IItem
{
    private GameObject _Storage;
    private PlayerInventory _PlayerInventory;
    private PlayerProperties _PlayerProperties;
    private GameObject _Bolt;
    private bool _Loaded = false;
    private string[] _BoltTypes = new string[2];

    public int Quantity {get; set;} = 1;
    public string CurrentBoltType = "Bolt";


    private bool CheckBoltsAvailability() {
        if (_PlayerInventory.Items.ContainsKey(CurrentBoltType)) {
            return true;
        } else {
            foreach (string BoltType in _BoltTypes) {
                if (_PlayerInventory.Items.ContainsKey(BoltType)) {
                    CurrentBoltType = BoltType;

                    return true;
                }
            }
        }
        
        return false;
    }

    private void LoadBolt() {
        if (CheckBoltsAvailability()) {
            StartCoroutine("LoadBoltAfterAnimation");
        }
    }
    
    void Start() {
        _Storage = GameObject.Find("Player").transform.Find("Storage").gameObject;
        _PlayerInventory = GameObject.Find("Player").GetComponent<PlayerInventory>();
        _PlayerProperties = GameObject.Find("Player").GetComponent<PlayerProperties>();

        _BoltTypes[0] = "SharpBolt";
        _BoltTypes[1] = "Bolt";
    }

    public void OnKeyDown() {
        if (transform.Find("Bolt") == null) {
            LoadBolt();
        }
    }

    public void OnKey() {
        Time.timeScale = 0.5f;
        Time.fixedDeltaTime *= Time.timeScale;
    }

    public void OnKeyUp() {
        Time.timeScale = 1.0f;
        Time.fixedDeltaTime *= Time.timeScale;

        if (_Loaded) {
            _Bolt.GetComponent<IWeapon>().Striking = true;
            
            _Bolt.GetComponent<Rigidbody>().useGravity = true;
            _Bolt.GetComponent<Rigidbody>().AddForce(_Bolt.transform.forward * 25, ForceMode.Impulse);
            
            _Loaded = false;
        }
        
        LoadBolt();
    }

    public void PlaceInStorage() {
        if (transform.parent != null) {
            if (transform.parent.gameObject.GetComponent<HandSlots>() != null) {
                transform.parent.gameObject.GetComponent<HandSlots>().Slots[0] = transform.parent.transform.name;
            }
        }

        transform.parent = _Storage.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.Euler(0, 0, 0);
    }

    public void PlaceInHand(GameObject Hand) {
        Hand.GetComponent<HandSlots>().Slots[0] = transform.name;
        transform.parent = Hand.transform;
        transform.localPosition = Vector3.zero;

        if (Hand.transform.name == "LeftHand") {
            transform.localRotation = Quaternion.Euler(-70f, 0, 0);
        } else {
            transform.localRotation = Quaternion.Euler(-70f, -70f, 0);
        }
    }

    public void AddToInventory() {
        PlaceInStorage();
        _PlayerInventory.Items.Add(transform.name, this);
        gameObject.tag = "Item";
        gameObject.GetComponent<BoxCollider>().isTrigger = false;
    }

    public void RemoveFromInventory(Transform NewParent = null) {
        transform.parent = NewParent;
        _PlayerInventory.Items.Remove(transform.name);
        gameObject.tag = "Interactable";
        gameObject.GetComponent<BoxCollider>().isTrigger = true;
    }

    IEnumerator LoadBoltAfterAnimation() {
        yield return new WaitForSeconds(_PlayerProperties.CrossbowLoadTime);

        //finish animation

        _PlayerInventory.Items[CurrentBoltType].RemoveFromInventory(transform);
        _Bolt = transform.Find("Bolt").gameObject;
        _Bolt.GetComponent<Rigidbody>().useGravity = false;
        _Bolt.transform.localPosition = Vector3.zero;
        _Bolt.transform.localRotation = Quaternion.Euler(0, 0, 0);

        _Loaded = true;
    }

}
