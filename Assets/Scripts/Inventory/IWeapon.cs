﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IWeapon {
    Transform transform {get;}

    GameObject gameObject {get;}

    float BaseDamage {get;}

    string DamageType {get;}

    string StrikeType {get;}

    float CurrentDamageMultiplier {get; set;}

    float MaxDamageMultiplier {get;}

    bool Striking {get; set;}
}
