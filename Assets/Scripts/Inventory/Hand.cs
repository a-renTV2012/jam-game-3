﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hand : MonoBehaviour, IItem, IWeapon
{
    private PlayerProperties _PlayerProperties;
    private EventPublisher _EventPublisher;
    private GameObject _Storage;

    public int Quantity {get; set;} = 1;
    public bool Striking {get; set;} = false;
    public float BaseDamage {get; set;} = 5;
    public float CurrentDamageMultiplier {get; set;} = 1;
    public float MaxDamageMultiplier {get; set;} = 2;
    public string StrikeType {get;} = "Common";
    public string DamageType {get; set;} = "Physical";


    private void OnMeleeDamageChange(float Multiplier) {
        BaseDamage *= Multiplier;
    }

    void Start() {
        _PlayerProperties = GameObject.Find("Player").GetComponent<PlayerProperties>();
        _Storage = GameObject.Find("Player").transform.Find("Storage").gameObject;
        _EventPublisher = GameObject.FindObjectOfType<EventPublisher>();

        _EventPublisher.MeleeDamageChangeEvent.AddListener(OnMeleeDamageChange);
    }

    public void OnKeyDown() {
        CurrentDamageMultiplier = 1;
    }

    public void OnKey() {
        if (CurrentDamageMultiplier < MaxDamageMultiplier) {
            CurrentDamageMultiplier += 1 * Time.deltaTime;
        }
    }

    public void OnKeyUp() {
        Striking = true;

        if (CurrentDamageMultiplier < 1.5f) {
            // light strike animation
            StartCoroutine("FinishLightStrikeAfterAnimation");
        } else {
            // hard strike animation
            StartCoroutine("FinishHardStrikeAfterAnimation");
        }
    }

    public void PlaceInStorage() {}

    public void PlaceInHand(GameObject Hand) {
        Hand.GetComponent<HandSlots>().Slots[0] = transform.name;
    }

    public void AddToInventory() {}

    public void RemoveFromInventory(Transform NewParent = null) {}

    IEnumerator FinishLightStrikeAfterAnimation() {
        yield return new WaitForSeconds(_PlayerProperties.LightStrikeTime);

        //finish animation

        Striking = false;
    }

    IEnumerator FinishHardStrikeAfterAnimation() {
        yield return new WaitForSeconds(_PlayerProperties.HardStrikeTime);

        //finish animation

        Striking = false;
    }
}
