﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OscillatingLight : MonoBehaviour
{
    public Vector3 basicPosition;
    public float oscillationRange = 0.0003f;


    void Start() 
    {
        basicPosition = transform.localPosition;
    }


    void Update()
    {
        transform.localPosition = basicPosition + new Vector3(Random.Range(-oscillationRange, oscillationRange), Random.Range(-oscillationRange, oscillationRange), Random.Range(-oscillationRange, oscillationRange));
    }
}
