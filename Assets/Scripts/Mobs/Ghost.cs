﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : Mob, IMob
{
    private bool Striking = false;

    protected override void SetValues() {
        _Health = 50;
        MaxHealth = 50;

        DamageMultiplierChange = 0;

        ChoppingStrikeDamageMultiplier = 1;
        PiercingStrikeDamageMultiplier = 1;
        CommonStrikeDamageMultiplier = 1;

        PhysicalDamageTypeMultiplier = 0;
        FireDamageTypeMultiplier = 0.5f;
        EnchantedDamageTypeMultiplier = 1;

        SightRange = 16;

        IgnoreMask = 1 << 0 | 1 << 1 | 1 << 2 | 1 << 9 | 1 << 10 | 1 << 11;
    }

    protected override void OnPlayerInSight() {
        if (Vector3.Distance(transform.position, Player.transform.position) < 4 && !Striking) {
            GetComponent<Rigidbody>().AddForce(transform.forward * 10, ForceMode.Impulse);

            Striking = true;

            StartCoroutine("FinishAttackAfterCooldown");
        }
    }

    protected override void RunAdditionalCodeOnTriggerEnter(Collider Collider) {
        if (Collider.gameObject.GetComponent<CharacterController>() != null) {
            Collider.gameObject.GetComponent<PlayerProperties>().health -= 35;
        }
    }

    IEnumerator FinishAttackAfterCooldown() {
        yield return new WaitForSeconds(0.1f);

        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(transform.rotation.x, transform.rotation.y + 180, transform.rotation.z), 1 * Time.deltaTime);

        yield return new WaitForSeconds(1);

        Striking = false;
    }
}
