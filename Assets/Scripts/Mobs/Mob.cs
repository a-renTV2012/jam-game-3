﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Mob : MonoBehaviour, IMob {
    protected NavMeshAgent NavMeshAgent;
    protected GameObject Player;

    protected bool HearPlayer = false;
    protected bool Chasing = false;
    protected int ChasingTimer = 30;

    protected float _Health = 100;

    protected int PlayerLayerMask = 1 << 11;
    protected int IgnoreMask = 1 << 1 | 1 << 2 | 1 << 10 | 1 << 11;

    public float Health {
        get {
            return _Health;
        }

        protected set {
            if (value > MaxHealth) {
                _Health = MaxHealth;
            } else {
                _Health = value;

                if (_Health <= 0) { Object.Destroy(gameObject); }
            }
        }
    }
    public float MaxHealth {get; protected set;} = 100;

    public float DamageMultiplierChange {get; protected set;} = -1;

    public float PhysicalDamageTypeMultiplier {get; protected set;} = 1;
    public float FireDamageTypeMultiplier {get; protected set;} = 1;
    public float EnchantedDamageTypeMultiplier {get; protected set;} = 1;

    public float ChoppingStrikeDamageMultiplier {get; protected set;} = 1;
    public float PiercingStrikeDamageMultiplier {get; protected set;} = 1;
    public float CommonStrikeDamageMultiplier {get; protected set;} = 1;

    public float SightRange {get; protected set;} = 128;


    protected virtual void SetValues() {}

    protected virtual void RunAdditionalCodeOnTriggerEnter(Collider Collider) {}

    protected virtual void RunAdditionalCodeOnWeaponTriggerEnter(Collider Collider) {}

    protected virtual bool CheckForPlayerInSight() {
        if (Vector3.Angle(transform.forward, Player.transform.position - transform.position) < 90 && Vector3.Distance(Player.transform.position, transform.position) < SightRange && !Physics.Linecast(transform.position, Player.transform.position, ~IgnoreMask)) {
            return true;
        }

        return false;
    }

    protected virtual void OnPlayerInSight() {
        if (!Chasing) {
            StartChasing();
        }

        ChasingTimer = 30;
    }

    protected Vector3 FindRandomPointOnNavMesh(int Range) {
        NavMeshHit RandomPoint = default(NavMeshHit);
        Vector3 SamplePoint = new Vector3();
        SamplePoint = Random.insideUnitCircle * Range;

        if (NavMesh.SamplePosition(transform.position + SamplePoint, out RandomPoint, 1, NavMesh.AllAreas)) {
            return RandomPoint.position;
        } else {
            return FindRandomPointOnNavMesh(Range);
        }
    }

    protected void StartChasing() {
        Chasing = true;

        InvokeRepeating("DecreaseChasingTimer", 0, 1);
    }

    protected void DecreaseChasingTimer() {
        ChasingTimer -= 1;

        if (ChasingTimer == 0) {
            CancelInvoke("DecreaseChasingTimer");
            Chasing = false;
        }
    }

    protected void Start() {
        SetValues();

        Player = GameObject.Find("Player");

        NavMeshAgent = GetComponent<NavMeshAgent>();
        NavMeshAgent.SetDestination(FindRandomPointOnNavMesh(100));
    }

    protected void Update() {
        if (CheckForPlayerInSight()) {
            OnPlayerInSight();

            NavMeshAgent.SetDestination(Player.transform.position);

        } else if (Chasing && NavMeshAgent.remainingDistance <= NavMeshAgent.stoppingDistance) {
            NavMeshAgent.SetDestination(FindRandomPointOnNavMesh(30));
        }

        if (NavMeshAgent.remainingDistance <= NavMeshAgent.stoppingDistance && !Chasing) {
            NavMeshAgent.SetDestination(FindRandomPointOnNavMesh(100));
        }
    }

    protected void OnTriggerEnter(Collider Collider) {
        IWeapon Weapon = Collider.gameObject.GetComponent<IWeapon>();

        if (Weapon != null) {
            if (Weapon.Striking) {
                float Damage = Weapon.BaseDamage * Weapon.CurrentDamageMultiplier;
                Weapon.CurrentDamageMultiplier += DamageMultiplierChange;

                if (Weapon.StrikeType == "Chopping") {
                    Damage *= ChoppingStrikeDamageMultiplier;
                } else if (Weapon.StrikeType == "Piercing") {
                    Damage *= PiercingStrikeDamageMultiplier;
                } else if (Weapon.StrikeType == "Common") {
                    Damage *= CommonStrikeDamageMultiplier;
                }

                if (Weapon.DamageType == "Physical") {
                    Damage *= PhysicalDamageTypeMultiplier;
                } else if (Weapon.DamageType == "Fire") {
                    Damage *= FireDamageTypeMultiplier;
                } else if (Weapon.DamageType == "Enchanted") {
                    Damage *= EnchantedDamageTypeMultiplier;
                }

                Health -= Damage;

                RunAdditionalCodeOnWeaponTriggerEnter(Collider);
            }
        }

        RunAdditionalCodeOnTriggerEnter(Collider);
    }

    public virtual void ReactToPlayerNoise(Vector3 PlayerPosition, int NoiseLevel) {
        HearPlayer = true;

        NavMeshAgent.SetDestination(PlayerPosition);

        if (!Chasing) {
            StartChasing();
        }
        
        ChasingTimer = 30;
    }
}