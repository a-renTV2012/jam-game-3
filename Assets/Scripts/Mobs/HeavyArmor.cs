﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyArmor : Armor, IArmor
{
    protected override void SetValues() {
        ChoppingStrikeDamageMultiplierChange = -1.5f;
        PiercingStrikeDamageMultiplierChange = -0.75f;
        CommonStrikeDamageMultiplierChange = -3;
    }
}
