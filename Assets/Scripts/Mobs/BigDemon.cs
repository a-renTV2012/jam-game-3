﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigDemon : Mob, IMob {
    protected override void SetValues() {
        _Health = 250;
        MaxHealth = 250;

        DamageMultiplierChange = 1;

        ChoppingStrikeDamageMultiplier = 0.75f;
        PiercingStrikeDamageMultiplier = 1;
        CommonStrikeDamageMultiplier = 0.25f;

        PhysicalDamageTypeMultiplier = 0.75f;
        FireDamageTypeMultiplier = 0.5f;
        EnchantedDamageTypeMultiplier = 1.5f;
    }
}
