﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMob {
    Transform transform {get;}

    GameObject gameObject {get;}

    float Health {get;}

    float MaxHealth {get;}

    float DamageMultiplierChange {get;}

    float PhysicalDamageTypeMultiplier {get;}

    float FireDamageTypeMultiplier {get;}

    float EnchantedDamageTypeMultiplier {get;}

    float ChoppingStrikeDamageMultiplier {get;}

    float PiercingStrikeDamageMultiplier {get;}

    float CommonStrikeDamageMultiplier {get;}

    float SightRange {get;}

    void ReactToPlayerNoise(Vector3 PlayerPosition, int NoiseLevel);
}
