﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IArmor
{
    Transform transform {get;}

    GameObject gameObject {get;}

    float ChoppingStrikeDamageMultiplierChange {get;}

    float PiercingStrikeDamageMultiplierChange {get;}
    
    float CommonStrikeDamageMultiplierChange {get;}
}
