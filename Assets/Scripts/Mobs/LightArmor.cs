﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightArmor : Armor, IArmor
{
    protected override void SetValues() {
        ChoppingStrikeDamageMultiplierChange = -0.5f;
        PiercingStrikeDamageMultiplierChange = -0.25f;
        CommonStrikeDamageMultiplierChange = -1;
    }
}
