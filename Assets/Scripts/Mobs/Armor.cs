﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Armor : MonoBehaviour, IArmor {
    public float ChoppingStrikeDamageMultiplierChange {get; protected set;} = -1;
    public float PiercingStrikeDamageMultiplierChange {get; protected set;} = -1;
    public float CommonStrikeDamageMultiplierChange {get; protected set;} = -1;


    protected virtual void SetValues() {}

    protected void Start() {
        SetValues();
    }

    protected void OnTriggerEnter(Collider Collider) {
        IWeapon Weapon = Collider.gameObject.GetComponent<IWeapon>();

        if (Weapon != null) {
            if (Weapon.Striking) {
                if (Weapon.StrikeType == "Chopping") {
                    Weapon.CurrentDamageMultiplier += ChoppingStrikeDamageMultiplierChange;
                } else if (Weapon.StrikeType == "Piercing") {
                    Weapon.CurrentDamageMultiplier += PiercingStrikeDamageMultiplierChange;
                } else if (Weapon.StrikeType == "Common") {
                    Weapon.CurrentDamageMultiplier += CommonStrikeDamageMultiplierChange;
                }
            }
        }
    }
}