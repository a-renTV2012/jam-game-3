﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Note : MonoBehaviour
{
    private StoryController _storyController;
    private EventPublisher _eventPublisher;
    public int number = 0;


    void Start()
    {
        _storyController = GameObject.FindObjectOfType<StoryController>();
        _eventPublisher = GameObject.FindObjectOfType<EventPublisher>();
        _eventPublisher.noteInteractionEvent.AddListener(onInteract);
    }

    
    void onInteract(string name = null)
    {
        if (name != null) {
            if (name == transform.name.Replace("(Clone)", "")) {
                Debug.Log(_storyController.getNote(number).text);
            }
        }
    }
}
