﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NoiseGeneration : MonoBehaviour
{
    private int _MobsLayerMask = 1 << 10;
    private int _ObstaclesMask = 1 << 1 | 1 << 2 | 1 << 10 | 1 << 11;

    public int NoiseLevel = 0;
    public float NoiseDistanceMultiplier = 1;


    void Start() {
        _ObstaclesMask = ~_ObstaclesMask;
        
    }
    
    void Update() {
        float NoiseRadius = 0;

        if (NoiseLevel == 1) {
            NoiseRadius = 10 * NoiseDistanceMultiplier;
        } else if (NoiseLevel == 2) {
            NoiseRadius = 24 * NoiseDistanceMultiplier;
        } else if (NoiseLevel == 3) {
            NoiseRadius = 32 * NoiseDistanceMultiplier;
        } else if (NoiseLevel == 4) {
            NoiseRadius = 64 * NoiseDistanceMultiplier;
        }

        Collider[] MobHits = new Collider[16];
        
        int iMax = Physics.OverlapSphereNonAlloc(transform.position, NoiseRadius, MobHits, _MobsLayerMask);

        if (iMax != 0) {
            for (int i= 0; i < iMax; i++) {
                if (!Physics.Linecast(transform.position, MobHits[i].transform.position, _ObstaclesMask) || NoiseLevel == 3 || NoiseLevel == 4) {
                    MobHits[i].gameObject.GetComponent<IMob>().ReactToPlayerNoise(transform.position, NoiseLevel);
                }
            }
        }
    }
}
