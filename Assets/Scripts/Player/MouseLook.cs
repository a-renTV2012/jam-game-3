﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLook : MonoBehaviour
{
    public float sensitivity = 9.0f;

    public float minY = -60.0f;
    public float maxY = 85.0f;

    private float rotationX = 0.0f;
    private float rotationY = 0.0f;

    private Transform Camera;

    
    void Start()
    {
        Camera = transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head/MainCamera");
    }

    
    void Update()
    {
        rotationX -= Input.GetAxis("Mouse Y") * sensitivity;
        rotationX = Mathf.Clamp(rotationX, minY, maxY);

        float delta = Input.GetAxis("Mouse X") * sensitivity;
        rotationY = transform.localEulerAngles.y + delta;

        transform.localEulerAngles = new Vector3(0, rotationY, 0);
        Camera.localEulerAngles = new Vector3(rotationX, 0, 0);
    }
}
