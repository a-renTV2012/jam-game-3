﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActions : MonoBehaviour
{
    //private Animator _animator;
    private GameObject _hitObject = null;
    private EventPublisher _eventPublisher;
    private Camera _camera;
    private PlayerInventory _PlayerInventory;
    private GameObject _currentTorch;
    private GameObject _leftHand;
    private GameObject _rightHand;
    private float _FHoldCounter = 0;


    void Start()
    {
        _eventPublisher = GameObject.FindObjectOfType<EventPublisher>();
        _camera = transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:Neck/mixamorig:Head/MainCamera").GetComponent<Camera>();
        _PlayerInventory = GetComponent<PlayerInventory>();
        _leftHand = transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:LeftShoulder/mixamorig:LeftArm/mixamorig:LeftForeArm/mixamorig:LeftHand/LeftHand").gameObject;
        _rightHand = transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:RightShoulder/mixamorig:RightArm/mixamorig:RightForeArm/mixamorig:RightHand/RightHand").gameObject;
        
        //_animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)) {
            Vector3 point = new Vector3(_camera.pixelWidth / 2, _camera.pixelHeight / 2, 0);

            Ray ray = _camera.ScreenPointToRay(point);

            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 2)) {
                _hitObject = hit.transform.gameObject;   
            } else {
                _hitObject = null;
            }
        }

        if (Input.GetKeyUp(KeyCode.F)) {
            if (_FHoldCounter < 0.11f) {
                if (_hitObject != null) {
                    if (_hitObject.tag == "Interactable") {
                        if (_hitObject.name.Contains("Torch")) {
                            _hitObject.GetComponent<Torch>().AddToInventory();

                        } else if (_hitObject.name.Contains("Sword")) {
                            _hitObject.GetComponent<Sword>().AddToInventory();

                        }
                    }

                    _hitObject = null;
                }
            }

            _FHoldCounter = 0;
        }

        /*if (Input.GetKey(KeyCode.F)) {
            _FHoldCounter += 1 * Time.deltaTime;

            if (_FHoldCounter > 0.5f) {
                if (_hitObject != null) {
                    if (_hitObject.tag == "Interactable") {
                        if (_hitObject.name.Contains("Torch")) {
                            if (_currentTorch != null) {
                                _currentTorch.GetComponent<Torch>().StartTorchDisablingCoroutine();

                                //_animator.SetBool("Firing", true)
                                StartCoroutine("changeAfterFiringAnimation", _hitObject);
                            } else {
                                _currentTorch = _hitObject;
                                _currentTorch.GetComponent<Torch>().AddToInventory();
                                _currentTorch.GetComponent<Torch>().PlaceInHand(_leftHand);
                            }

                        } else if (_hitObject.name.Contains("Note")) {
                            _eventPublisher.invokeNoteInteractionEvent(_hitObject.name);

                        } else if (_hitObject.name.Contains("Sword")) {
                            
                            _hitObject.GetComponent<Sword>().AddToInventory();
                            _hitObject.GetComponent<Sword>().PlaceInHand(_rightHand);

                        }
                    }

                    _hitObject = null;
                }

                _FHoldCounter = 0;
            }
        }*/

        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            _PlayerInventory.Items[_rightHand.GetComponent<HandSlots>().Slots[0]].OnKeyDown();
        }

        if (Input.GetKeyUp(KeyCode.Mouse0)) {
            _PlayerInventory.Items[_rightHand.GetComponent<HandSlots>().Slots[0]].OnKeyUp();
        }

        if (Input.GetKey(KeyCode.Mouse0)) {
            _PlayerInventory.Items[_rightHand.GetComponent<HandSlots>().Slots[0]].OnKey();
        }

        if (Input.GetKeyDown(KeyCode.Mouse1)) {
            _PlayerInventory.Items[_leftHand.GetComponent<HandSlots>().Slots[0]].OnKeyDown();
        }

        if (Input.GetKeyUp(KeyCode.Mouse1)) {
            _PlayerInventory.Items[_leftHand.GetComponent<HandSlots>().Slots[0]].OnKeyUp();
        }

        if (Input.GetKey(KeyCode.Mouse1)) {
            _PlayerInventory.Items[_leftHand.GetComponent<HandSlots>().Slots[0]].OnKey();
        }
    }

    IEnumerator changeAfterFiringAnimation(GameObject takenTorch) {
        yield return new WaitForSeconds(2);

        //_animator.SetBool("Firing", false);

        takenTorch.GetComponent<Torch>().AddToInventory();

        takenTorch.transform.parent = _rightHand.transform;
        transform.localRotation = Quaternion.Euler(74, 0, 0);
        transform.localPosition = new Vector3(-0.2f, -3, 0.4f);

        //_animator.SetBool("ChangingTorch", true);

        StartCoroutine("transferAfterChangingAnimation", takenTorch);
    }

    IEnumerator transferAfterChangingAnimation(GameObject takenTorch) {
        yield return new WaitForSeconds(1);

        //_animator.SetBool("ChangingTorch", false)

        _currentTorch.GetComponent<Torch>().RemoveFromInventory();

        _currentTorch.transform.parent = null;
        _currentTorch.transform.position = _leftHand.transform.position;
        _currentTorch.transform.rotation = Quaternion.Euler(-90f, 0, 0);
        
        _currentTorch = takenTorch;

        //_animator.SetBool("TransferingTorch", true);

        StartCoroutine("retreatAfterTransferingAnimation");
    }

    IEnumerator retreatAfterTransferingAnimation() {
        yield return new WaitForSeconds(1);

        //_animator.SetBool("TransferingTorch", false);

        _currentTorch.GetComponent<Torch>().PlaceInHand(_leftHand);

        //_animator.SetBool("RetreatingAfterTransferingTorch", true);

        StartCoroutine("closeRetreatingAnimation");
    }

    IEnumerator closeRetreatingAnimation() {
        yield return new WaitForSeconds(0.3f);

        //_animator.SetBool("RetreatingAfterTransferingTorch", false);
    }
}
