using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerProperties : MonoBehaviour
{   
    private AudioSource _Heartbeat;
    private GameUI _UIManager;

    private float _stamina = 100f;
    private float _health = 100f;

    public int MaxStamina = 100;
    public int MaxHealth = 100;
    public float StaminaRechargeRate = 5;
    public float StaminaRechargeCooldown = 3;
    public float crouchSpeed = 2;
    public float moveSpeed = 4;
    public float sprintSpeed = 7;
    public float jumpSpeed = 10;
    public float boundSpeed = 15;
    public float BoundTime = 0.6f;
    public float LightStrikePrepareTime = 0.3f;
    public float HardStrikePrepareTime = 0.5f;
    public float LightStrikeTime = 1;
    public float HardStrikeTime = 2;
    public float CrossbowLoadTime = 3;

    public float endOfActions;

    public float health {
        get {
            return _health;
        }

        set {
            if (value > MaxHealth) {
                _health = MaxHealth;
            } else {
                if (value < _health) {
                    _Heartbeat.Play();
                }

                _health = value;
                
                if (_health <= 0) { 
                    Object.Destroy(gameObject); 

                    _UIManager.TogglePrimaryButton();
                    _UIManager.ChangeMainMenuState();
                }

                _UIManager.SetHealthBarWidth(value);
            }
        }
    }

    public float stamina {
        get {
            return _stamina;
        }

        set {
            if (value < 0) {
                _stamina = 0;
            } else if (value > MaxStamina) {
                _stamina = MaxStamina;
            } else {
                _stamina = value;
            }

            _UIManager.SetStaminaBarWidth(value);
        }
    }


    void Start() 
    {
        _Heartbeat = transform.Find("Heartbeat").gameObject.GetComponent<AudioSource>();

        _UIManager = GameObject.Find("UIManager").GetComponent<GameUI>();

        endOfActions = Time.time;
    }


    void Update()
    {
        if (endOfActions + StaminaRechargeCooldown < Time.time && stamina < MaxStamina) {
            stamina += StaminaRechargeRate * Time.deltaTime;
        }
    }
}
