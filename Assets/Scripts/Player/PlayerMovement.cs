﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
[RequireComponent(typeof(PlayerProperties))]

public class PlayerMovement : MonoBehaviour
{
    private PlayerProperties _playerProperties;
    private NoiseGeneration _NoiseGeneration;
    private CharacterController _characterController;

    private AudioSource _AudioSource;
    private AudioClip _CrouchingClip;
    private AudioClip _WalkingClip;
    private AudioClip _SprintingClip;
    private AudioClip _JumpClip;
    private AudioClip _StepClip;
    
    private Rigidbody _rb;
    private ControllerColliderHit _contact;
    private Animator _Animator;

    private float _verticalSpeed;
    private bool _HitGround = false;
    private bool _Crouching = false;

    public float rotationSpeed = 15f;
    public float gravity = -9.81f;
    public float terminalVelocity = -20f;
    public float minFall = -1.5f;
    public float pushForce = 3f;
    public string boundDirection = "";
    public int boundStatus = 0;


    void Start() {
        _playerProperties = GetComponent<PlayerProperties>();
        _NoiseGeneration = GetComponent<NoiseGeneration>();
        _characterController = GetComponent<CharacterController>();

        _AudioSource = GetComponent<AudioSource>();
        _CrouchingClip = Resources.Load<AudioClip>("Audio/Crouching");
        _WalkingClip = Resources.Load<AudioClip>("Audio/Walking");
        _SprintingClip = Resources.Load<AudioClip>("Audio/Sprinting");
        _JumpClip = Resources.Load<AudioClip>("Audio/Jump");
        _StepClip = Resources.Load<AudioClip>("Audio/Step");
        _AudioSource.clip = _StepClip;

        _rb = GetComponent<Rigidbody>();
        _Animator = GetComponent<Animator>();
        _verticalSpeed = minFall;
    }


    void Update()
    {
        bool tmp = _HitGround;
        _HitGround = false;
        RaycastHit hit;

        if (_verticalSpeed < 0 && Physics.Raycast(transform.position, Vector3.down, out hit)) {
            float check = (_characterController.height + _characterController.radius) / 1.9f;
            _HitGround = hit.distance <= check;

            if (_HitGround && !tmp) {
                if (_AudioSource.clip != _StepClip) {
                    _AudioSource.Stop();
                    _AudioSource.clip = _StepClip;
                    _AudioSource.volume = 1;
                    _AudioSource.loop = false;
                    _AudioSource.Play();
                }
            }
        }

        _NoiseGeneration.NoiseLevel = 0;

        Vector3 movement = Vector3.zero;

        _Animator.SetInteger("Speed", 0);

        if (Input.GetKeyDown(KeyCode.LeftControl)) {
            _Crouching = !_Crouching;
            _Animator.SetBool("Crouching", _Crouching);
        }

        if (boundStatus == 0) {
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");

            if (verticalInput != 0 || horizontalInput != 0 || Input.GetKey(KeyCode.Space)) {
                movement.x = horizontalInput;
                movement.z = verticalInput;

                if (Input.GetKey(KeyCode.LeftAlt) && _playerProperties.stamina > 30) {
                    /*boundStatus = 1;

                    if (Input.GetKey(KeyCode.S)) {
                        boundDirection = "S";
                    } else if (Input.GetKey(KeyCode.A)) {
                        boundDirection = "A";
                    } else if (Input.GetKey(KeyCode.D)) {
                        boundDirection = "D";
                    } else if (Input.GetKey(KeyCode.W)) {
                        boundDirection = "W";
                    }
                    //_animator.SetFloat("speed", _playerProperties.boundSpeed);

                    StartCoroutine("bound");

                    _playerProperties.stamina -= 25;
                    _playerProperties.endOfActions = Time.time;

                    if (_AudioSource.clip != _JumpClip) {
                        _AudioSource.Stop();
                        _AudioSource.clip = _JumpClip;
                        _AudioSource.volume = 0.5f;
                        _AudioSource.loop = false;
                        _AudioSource.Play();
                    }*/

                } else {
                    if (_Crouching) {
                        movement = Vector3.ClampMagnitude(movement * _playerProperties.crouchSpeed, _playerProperties.crouchSpeed);

                        _NoiseGeneration.NoiseLevel = 1;

                        if (_AudioSource.clip != _CrouchingClip && _HitGround) {
                            _AudioSource.Stop();
                            _AudioSource.clip = _CrouchingClip;
                            _AudioSource.volume = 0.2f;
                            _AudioSource.loop = true;
                            _AudioSource.Play();
                        }

                        _Animator.SetInteger("Speed", 1);

                    } else if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.W) && _playerProperties.stamina > 4.99f) {
                        movement = Vector3.ClampMagnitude(movement * _playerProperties.sprintSpeed, _playerProperties.sprintSpeed);
                        _playerProperties.stamina -= 5 * Time.deltaTime;
                        _playerProperties.endOfActions = Time.time;

                        _NoiseGeneration.NoiseLevel = 3;

                        if (_AudioSource.clip != _SprintingClip && _HitGround) {
                            _AudioSource.Stop();
                            _AudioSource.clip = _SprintingClip;
                            _AudioSource.volume = 1;
                            _AudioSource.loop = true;
                            _AudioSource.Play();
                        }

                        _Animator.SetInteger("Speed", 3);

                    } else {
                        movement = Vector3.ClampMagnitude(movement * _playerProperties.moveSpeed, _playerProperties.moveSpeed);

                        _NoiseGeneration.NoiseLevel = 2;

                        if (_AudioSource.clip != _WalkingClip && _HitGround) {
                            _AudioSource.Stop();
                            _AudioSource.clip = _WalkingClip;
                            _AudioSource.volume = 0.8f;
                            _AudioSource.loop = true;
                            _AudioSource.Play();
                        }

                        _Animator.SetInteger("Speed", 2);
                    }

                    movement = transform.TransformDirection(movement);
                }
            }
        } else if (boundStatus == 1 && _HitGround) {
            if (boundDirection == "S") {
                movement.z = -1;
            } else if (boundDirection == "A") {
                movement.x = -1;
            } else if (boundDirection == "D") {
                movement.x = 1;
            } else if (boundDirection == "W") {
                movement.z = 1;
            }

            movement = transform.TransformDirection(Vector3.ClampMagnitude(movement * _playerProperties.boundSpeed, _playerProperties.boundSpeed));

            _NoiseGeneration.NoiseLevel = 3;
        }

        if (_HitGround) {
            if (Input.GetButtonDown("Jump") && _playerProperties.stamina > 9.99f) {
                _verticalSpeed = _playerProperties.jumpSpeed;

                _playerProperties.stamina -= 15;
                _playerProperties.endOfActions = Time.time;

                _NoiseGeneration.NoiseLevel = 3;

                if (_AudioSource.clip != _JumpClip) {
                    _AudioSource.Stop();
                    _AudioSource.clip = _JumpClip;
                    _AudioSource.volume = 0.5f;
                    _AudioSource.loop = false;
                    _AudioSource.Play();
                }

                _Animator.SetBool("Jumping", true);
                StartCoroutine("FinishJumpingAnimation");

            } else {
                _verticalSpeed = minFall;
            }
        } else {
            _verticalSpeed += gravity * 5 * Time.deltaTime;

            if (_verticalSpeed < terminalVelocity) {
                _verticalSpeed = terminalVelocity;
            }

            if (_characterController.isGrounded) {
                if (Vector3.Dot(movement, _contact.normal) < 0) {
                    movement = _contact.normal * _playerProperties.moveSpeed;
                } else {
                    movement += _contact.normal * _playerProperties.moveSpeed;
                }

                if (_AudioSource.clip != _StepClip) {
                    _AudioSource.Stop();
                    _AudioSource.clip = _StepClip;
                    _AudioSource.volume = 1;
                    _AudioSource.loop = false;
                    _AudioSource.Play();
                }
            }
        }

        movement.y = _verticalSpeed;

        if (movement.x == 0 && movement.z == 0 && movement.y < 0) {
            if (_AudioSource.clip != _StepClip) {
                _AudioSource.Stop();
                _AudioSource.clip = null;
            } 
        }

        _characterController.Move(movement * Time.deltaTime);
    }


    void OnControllerColliderHit(ControllerColliderHit hit) {
        _contact = hit;

        Rigidbody body = hit.collider.attachedRigidbody;

        if (body != null && !body.isKinematic) {
            body.velocity = hit.moveDirection * pushForce * _characterController.velocity.magnitude;
        }
    }


    IEnumerator bound() {
        yield return new WaitForSeconds(_playerProperties.BoundTime);

        boundStatus = 2;
        //_animator.SetFloat("speed", 0);
        //_animator.SetBool("balancing", true);

        if (_AudioSource.clip != _StepClip) {
            _AudioSource.Stop();
            _AudioSource.clip = _StepClip;
            _AudioSource.volume = 1;
            _AudioSource.loop = false;
            _AudioSource.Play();
        }

        StartCoroutine("balancing");
    }


    IEnumerator balancing() {
        yield return new WaitForSeconds(_playerProperties.BoundTime);

        boundStatus = 0;
        //_animator.SetBool("balancing", false);
    }

    IEnumerator FinishJumpingAnimation() {
        yield return new WaitForSeconds(0.75f);

        _Animator.SetBool("Jumping", false);
    }
}