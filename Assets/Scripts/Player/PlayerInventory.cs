﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    private float _HoldCounter = 0;
    private GameObject _LeftHand;
    private GameObject _RightHand;
    private GameObject _Storage;

    public Dictionary<string, IItem> Items = new Dictionary<string, IItem>();


    void Start() {
        _LeftHand = transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:LeftShoulder/mixamorig:LeftArm/mixamorig:LeftForeArm/mixamorig:LeftHand/LeftHand").gameObject;
        _RightHand = transform.Find("mixamorig:Hips/mixamorig:Spine/mixamorig:Spine1/mixamorig:Spine2/mixamorig:RightShoulder/mixamorig:RightArm/mixamorig:RightForeArm/mixamorig:RightHand/RightHand").gameObject;
        _Storage = transform.Find("Storage").gameObject;
        Items.Add("LeftHand", _LeftHand.GetComponent<Hand>());
        Items.Add("RightHand", _RightHand.GetComponent<Hand>());

        //TEMP!!!
        Items.Add("Sword", _RightHand.transform.Find("Sword").gameObject.GetComponent<IItem>());
        Items.Add("Torch", _LeftHand.transform.Find("Torch").gameObject.GetComponent<IItem>());
    }

    /*void Update() {
        if (Input.GetKey(KeyCode.Q) || Input.GetKey(KeyCode.E)) {
            _HoldCounter += 1f * Time.deltaTime;
        }

        if (Input.GetKeyUp(KeyCode.Q)) {
            if (_HoldCounter <= 0.11f) {
                _LeftHand.GetComponent<HandSlots>().CycleSlots();
            }

            if (!Input.GetKey(KeyCode.E)) {
                _HoldCounter = 0;
            }
        }

        if (Input.GetKeyUp(KeyCode.E)) {
            if (_HoldCounter <= 0.11f) {
                _RightHand.GetComponent<HandSlots>().CycleSlots();
            }

            if (!Input.GetKey(KeyCode.Q)) {
                _HoldCounter = 0;
            }
        }
    }*/
}
