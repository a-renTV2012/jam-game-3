using UnityEngine;
using System.Collections;

public class CullingFog : MonoBehaviour
{
    void Start()
    {
        Camera camera = GetComponent<Camera>();
        float[] distances = new float[32];
        distances[1] = 100;
        camera.layerCullDistances = distances;
    }
}